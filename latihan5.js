var personArr = ["John", "Doe", "male", 27]
var personObj = {
    firstName : "John",
    lastName: "Doe",
    gender: "male",
    age: 27
}
 
console.log(personArr[0]) // John
console.log(personObj.firstName) // John 

var array = [ 1, 2, 3 ] 
console.log(typeof array) // object