/*

  A. Descending Ten (10 poin)
    Function DescendingTen adalah kebalikan dari function AscendingTen. 
    Output yang diharapkan adalah deretan angka dimulai dari angka parameter hingga 10 angka di bawahnya. 
    Function akan mengembalikan nilai -1 jika tidak ada parameter yang diberikan.

    contoh: 
    console.log(DescendingTen(10)) akan menampilkan 10 9 8 7 6 5 4 3 2 1
    console.log(DescendingTen(20)) akan menampilkan 20 19 18 17 16 15 14 13 12 11

    Hint: Deretan angka yang menjadi output adalah dalam tipe data String, bukan Number.
  
  B. Ascending Ten (10 poin)

    Buatlah sebuah function dengan nama AscendingTen yang menerima sebuah parameter berupa Number, 
    function AscendingTen tersebut akan mengembalikan deretan angka yang ditampilkan 
    dalam satu baris (ke samping). Deret angka yang ditampilkan adalah deretan angka 
    mulai dari angka yang menjadi parameter input function hingga 10 angka setelahnya yang dipisah dengan karakter spasi. 
    Function akan mengembalikan nilai -1 jika tidak ada parameter yang diberikan.
    contoh: 
    console.log(AscendingTen(1)) akan menampilkan 1 2 3 4 5 6 7 8 9 10  
    console.log(Ascending(101)) akan menampilkan 101 102 103 104 105 106 107 108 109 110

    Hint: Deretan angka yang menjadi output adalah dalam tipe data String, bukan Number.

  C. Conditional Ascending Descending (15)

    Tulislah sebuah function ConditionalAscDesc yang menerima dua buah parameter dengan tipe Number. 
    Parameter number pertama diberi nama reference, dan parameter number kedua diberi nama check. 
    Function ini mirip seperti kedua function sebelumnya yaitu AscendingTen 
    dan DescendingTen yaitu akan menampilkan 10 angka berderet dimulai atau diakhiri dari reference. 
    Function ConditionalAscDesc mengecek jika parameter check merupakan ganjil 
    maka output yang ditampilkan yaitu deretan angka ascending, 
    jika parameter check merupakan angka genap maka output yang ditampilkan yaitu deretan angka descending. 
    Function akan mengembalikan nilai -1 jika tidak ada parameter yang diberikan atau hanya satu saja parameter yang diberikan.

    Contoh: 
    console.log(ConditionalAscDesc(1, 1)) akan menampilkan 1 2 3 4 5 6 7 8 9 10
    console.log(ConditionalAscDesc(100, 4)) akan menampilkan 100 99 98 97 96 95 94 93 92 91

  D. Papan Ular Tangga (35)
    Buatlah sebuah function ularTangga yang ketika function tersebut dipanggil akan menampilkan papan ular tangga ukuran 10 x 10. 

    Output: 
    100 99 98 97 96 95 94 93 92 91
    81 82 83 84 85 86 87 88 89 90
    80 79 78 77 76 75 74 73 72 71
    61 62 63 64 65 66 67 68 69 70
    60 59 58 57 56 55 54 53 52 51
    41 42 43 44 45 46 47 48 49 50
    40 39 38 37 36 35 34 33 32 31
    21 22 23 24 25 26 27 28 29 30
    20 19 18 17 16 15 14 13 12 11
    1 2 3 4 5 6 7 8 9 10

*/

function DescendingTen(num) {
  var output = []
  var ten = num-10
  
  if(num == undefined){
    return -1
  }else{
    for(var i = num; i>ten; i--){
      output.push(i)
    }
    return output
  }

  }

function AscendingTen(num2) {
  var output = []
  var ten = num2+10
  
  if(num2 == undefined){
    return -1
  }else{
    for(var j = num2; j<ten; j++){
      output.push(j)
    }
    return output
  }
}

function ConditionalAscDesc(reference, check) {
  var output = []

  if(reference == undefined || check == undefined){
    return -1
  }else{
    if(reference%2!=0 && check%2!=0){
      var ten1 = reference+10
        for(var j = reference; j<ten1; j++){
          output.push(j)
        }
        return output
    }else{
      var ten2 = reference-10
      for(var j = reference; j>ten2; j--){
        output.push(j)
      }
      return output
    }
  }

}

function ularTangga() {
  var ular = '';
  var angka
  var a = 0
  var nilai_akhir = 100
  var batas
  for(var i = 10; i >= 1; i--) {
    
      if(nilai_akhir == 100){  
        angka = 100
      }else if(nilai_akhir == 91){
        angka = 81
      }else if(nilai_akhir == 90){
        angka = 80
      }else if(nilai_akhir == 71){
        angka = 61
      }else if(nilai_akhir == 70){
        angka = 60
      }else if(nilai_akhir == 51){
        angka = 41
      }else if(nilai_akhir == 50){
        angka = 40
      }else if(nilai_akhir == 41){
        angka = 31
      }else if(nilai_akhir == 40){
        angka = 30
      }else if(nilai_akhir == 31){
        angka = 21
      }else if(nilai_akhir == 30){
        angka = 20
      }else if(nilai_akhir == 21){
        angka = 11
      }else if(nilai_akhir == 20){
        angka = 10
      }else if(nilai_akhir == 11){
        angka = 1
      }else if(nilai_akhir == 10){
        angka = 0
      }


      if(nilai_akhir%2==1){
        batas = angka+10
        for(a = angka; a < 100; a++) {
          if((a+1)<=batas){

            ular += a+' ';

            nilai_akhir = a
          }

        }
      }else{
        batas = angka-10
        for(a = angka; a > 0; a--) {
          if((a-1)>=batas){

            ular += a+' ';

            nilai_akhir = a
          }

        }
      }

        ular += '\n';

    } 
  console.log(ular);
}

console.log("===============SOAL A===============");
// TEST CASES Descending Ten
console.log(DescendingTen(100,10)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1

console.log("===============SOAL B===============");
// TEST CASES Ascending Ten
console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1

console.log("===============SOAL C===============");
// TEST CASES Conditional Ascending Descending
console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1

console.log("===============SOAL D===============");
// TEST CASE Ular Tangga
console.log(ularTangga())
/*
Output :
  100 99 98 97 96 95 94 93 92 91
  81 82 83 84 85 86 87 88 89 90
  80 79 78 77 76 75 74 73 72 71
  61 62 63 64 65 66 67 68 69 70
  60 59 58 57 56 55 54 53 52 51
  41 42 43 44 45 46 47 48 49 50
  40 39 38 37 36 35 34 33 32 31
  21 22 23 24 25 26 27 28 29 30
  20 19 18 17 16 15 14 13 12 11
  1 2 3 4 5 6 7 8 9 10
*/
