//Soal If-Case

function soalIfElse(nama, peran){
    if (nama == ''){
        console.log('Nama harus diisi!');
    }else if(nama && peran == ''){
        console.log('Halo ' + nama + ', Pilih peranmu untuk memulai game!');
    }else if(nama == 'Jane' && peran == 'Penyihir'){
        console.log('Selamat datang di Dunia Werewolf ' + nama + '\nHalo ' + peran + ' ' + nama + ', kamu dapat melihat siapa yang menjadi werewolf!');
    }else if(nama == 'Jenita' && peran == 'Guard'){
        console.log('Selamat datang di Dunia Werewolf ' + nama + '\nHalo ' + peran + ' ' + nama + ', kamu akan membantu melindungi temanmu dari serangan werewolf!');
    }else if(nama == 'Junaedi' && peran == 'Werewolf'){
        console.log('Selamat datang di Dunia Werewolf ' + nama + '\nHalo ' + peran + ' ' + nama + ', Kamu akan memakan mangsa setiap malam!');
    }
}
console.log('________', 'Soal If Else 1', '________'); //Membuat judul
soalIfElse('','');
console.log('________', 'Soal If Else 2', '________'); //Membuat judul
soalIfElse('John','');
console.log('________', 'Soal If Else 3', '________'); //Membuat judul
soalIfElse('Jane','Penyihir');
console.log('________', 'Soal If Else 4', '________'); //Membuat judul
soalIfElse('Jenita','Guard');
console.log('________', 'Soal If Else 5', '________'); //Membuat judul
soalIfElse('Junaedi','Werewolf');



//Soal Switch-Case

var tanggal = 21; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 1; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1945; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
var namaBulan;

console.log('\n\n________', 'Soal Switch Case', '________'); //Membuat judul

switch (true){
    case (tanggal <1 || tanggal >31):{ console.log('Input Tanggal Salah'); break; }    
    case (tahun <1900 || tahun >2200):{ console.log('Input Tahun Salah'); break; }
    case (bulan <1 || bulan >12):{ console.log('Input Bulan Salah'); break; }
    default: 
        switch (bulan){
            case 1: 
                namaBulan="Januari";console.log(tanggal+" "+namaBulan+" "+tahun);break;
            case 2: 
                namaBulan="Februari";console.log(tanggal+" "+namaBulan+" "+tahun);break;    
            case 3: 
                namaBulan="Maret";console.log(tanggal+" "+namaBulan+" "+tahun);break;   
            case 4: 
                namaBulan="April";console.log(tanggal+" "+namaBulan+" "+tahun);break;   
            case 5: 
                namaBulan="Mei";console.log(tanggal+" "+namaBulan+" "+tahun);break;   
            case 6: 
                namaBulan="Juni";console.log(tanggal+" "+namaBulan+" "+tahun);break;   
            case 7: 
                namaBulan="Juli";console.log(tanggal+" "+namaBulan+" "+tahun);break; 
            case 8: 
                namaBulan="Agustus";console.log(tanggal+" "+namaBulan+" "+tahun);break;   
            case 9: 
                namaBulan="September";console.log(tanggal+" "+namaBulan+" "+tahun);break;   
            case 10: 
                namaBulan="Oktober";console.log(tanggal+" "+namaBulan+" "+tahun);break;   
            case 11: 
                namaBulan="November";console.log(tanggal+" "+namaBulan+" "+tahun);break;   
            case 12: 
                namaBulan="Desember";console.log(tanggal+" "+namaBulan+" "+tahun);break;  
            default: console.log("nama Bulan tidak ada");break;  
        }
}
